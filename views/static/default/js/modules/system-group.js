$(document).ready(function(){
    var groupId = $("#table").attr("data-group-id");
    $('#table').bootstrapTable({
        url: '/user/group-topic-list-'+groupId,
        queryParams: "queryParams",
        search: true,
        toolbar: "#toolbar",
        sidePagination: "true",
        striped: true, // 是否显示行间隔色
        uniqueId: "ID",
        sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
        pageNumber: 1,                       //初始化加载第一页，默认第一页
        pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
        pageSize: "10",
        pagination: true, // 是否分页
        sortable: true, // 是否启用排序
        columns: [{
            field: 'title',
            title: '标题'
        },
            {
                field: 'name',
                title: '昵称'
            },
            {
                field: 'price',
                title: '角色'
            },
            {
                field: 'price',
                title: '操作',
                width: 120,
                align: 'center',
                valign: 'middle',
                formatter: actionFormatter,
            }],
        responseHandler: function (res) {
            /*$("#invite_count").html(res.count);//count  后端返回的非total、rows数据
            $("#invite_month").html(res.month);*/
            console.log("------------------"+res.total);
            return{
                "total":res.total,
                "rows":res.rows
            }
        }
    });
    //操作栏的格式化
    function actionFormatter(value, row, index) {
        var id = value;
        var result = "";
        result += "<a href='javascript:;' class='btn btn-xs green' onclick=\"EditViewById('" + id + "', view='view')\" title='查看'><span class='glyphicon glyphicon-search'></span></a>";
        result += "<a href='javascript:;' class='btn btn-xs blue' onclick=\"EditViewById('" + id + "')\" title='编辑'><span class='glyphicon glyphicon-pencil'></span></a>";
        result += "<a href='javascript:;' class='btn btn-xs red' onclick=\"DeleteByIds('" + id + "')\" title='删除'><span class='glyphicon glyphicon-remove'></span></a>";
        return result;
    }
});