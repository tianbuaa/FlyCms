package com.flycms.modules.group.controller.manage;

import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.group.domain.GroupUser;
import com.flycms.modules.group.domain.dto.GroupUserDTO;
import com.flycms.modules.group.service.IGroupUserService;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

/**
 * 群组和用户对应关系Controller
 * 
 * @author admin
 * @date 2020-09-27
 */
@RestController
@RequestMapping("/system/group/user")
public class GroupUserAdminController extends BaseController
{
    @Autowired
    private IGroupUserService groupUserService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增群组和用户对应关系
     */
    @PreAuthorize("@ss.hasPermi('group:user:add')")
    @PostMapping
    public AjaxResult add(@RequestBody GroupUser groupUser)
    {
        return groupUserService.insertGroupUser(groupUser);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除群组和用户对应关系
     */
/*    @PreAuthorize("@ss.hasPermi('group:user:remove')")
    @Log(title = "群组和用户对应关系", businessType = BusinessType.DELETE)
    @DeleteMapping("/{userIds}")
    public AjaxResult remove(@PathVariable Long[] userIds)
    {
        return toAjax(groupUserService.deleteGroupUserByIds(userIds));
    }*/


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改群组和用户对应关系
     */
    @PreAuthorize("@ss.hasPermi('group:user:edit')")
    @Log(title = "群组和用户对应关系", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody GroupUser groupUser)
    {
        return toAjax(groupUserService.updateGroupUser(groupUser));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询群组和用户对应关系列表
     */
    @PreAuthorize("@ss.hasPermi('group:user:list')")
    @GetMapping("/list")
    public TableDataInfo list(GroupUser groupUser,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "create_time") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<GroupUserDTO> pager = groupUserService.selectGroupUserPager(groupUser, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出群组和用户对应关系列表
     */
    @PreAuthorize("@ss.hasPermi('group:user:export')")
    @Log(title = "群组和用户对应关系", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(GroupUser groupUser,
            @RequestParam(defaultValue = "1") Integer pageNum,
            @RequestParam(defaultValue = "10") Integer pageSize,
            @Sort @RequestParam(defaultValue = "id") String sort,
            @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<GroupUserDTO> pager = groupUserService.selectGroupUserPager(groupUser, pageNum, pageSize, sort, order);
        ExcelUtil<GroupUserDTO> util = new ExcelUtil<GroupUserDTO>(GroupUserDTO.class);
        return util.exportExcel(pager.getList(), "user");
    }

    /**
     * 获取群组和用户对应关系详细信息
     */
/*    @PreAuthorize("@ss.hasPermi('group:user:query')")
    @GetMapping(value = "/{userId}")
    public AjaxResult getInfo(@PathVariable("userId") Long userId)
    {
        return AjaxResult.success(groupUserService.findGroupUserById(userId));
    }*/
}
