package com.flycms.modules.group.domain.dto;
import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

/**
 * 群组和用户对应关系数据传输对象 fly_group_user
 * 
 * @author admin
 * @date 2020-09-27
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class GroupUserDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 用户ID */
    @Excel(name = "用户ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;
    /** 群组ID */
    @Excel(name = "群组ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long groupId;
    /** 是否管理员 */
    @Excel(name = "是否管理员")
    @JsonSerialize(using = ToStringSerializer.class)
    private Integer isadmin;
    /** 是否创始人 */
    @Excel(name = "是否创始人")
    private Integer isfounder;
    /** 到期时间 */
    @Excel(name = "到期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;
    /** 是否禁用状态 */
    @Excel(name = "是否禁用")
    private Integer status;
}
