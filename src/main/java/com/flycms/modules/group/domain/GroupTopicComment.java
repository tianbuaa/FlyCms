package com.flycms.modules.group.domain;

import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 话题回复/评论对象 fly_group_topic_comment
 * 
 * @author admin
 * @date 2020-12-15
 */
@Data
public class GroupTopicComment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 自增评论ID */
    private Long id;
    /** 上级评论ID */
    private Long referId;
    /** 小组ID */
    private Long groupId;
    /** 话题ID */
    private Long topicId;
    /** 用户ID */
    private Long userId;
    /** 回复内容 */
    private String content;
    /** 回复统计 */
    private Integer countComment;
    //顶
    private Integer countDigg;
    //踩
    private Integer countBurys;
    /** 楼层数量 */
    private Integer storey;
    //权重
    private Double weight;
    /** 0公开1不公开（仅自己和发帖者可看） */
    private Integer ispublic;
    /** 审核状态 */
    private Integer status;
    /** 删除 */
    private Integer deleted;
}
