package com.flycms.modules.group.domain.dto;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 群组(小组)数据传输对象 fly_group
 * 
 * @author admin
 * @date 2020-09-25
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class GroupDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 小组ID */
    @Excel(name = "小组ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    //短域名
    @Excel(name = "短域名")
    private String shortUrl;
    /** 用户ID */
    @Excel(name = "用户ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;
    /** 分类ID */
    @Excel(name = "分类ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long columnId;
    /** 群组名称 */
    @Excel(name = "群组名称")
    private String groupName;
    /** 小组介绍 */
    @Excel(name = "群组介绍")
    private String groupDesc;
    /** 图标路径 */
    @Excel(name = "图标路径")
    private String path;
    /** 小组图标 */
    @Excel(name = "小组图标")
    private String photo;
    /** 帖子统计 */
    @Excel(name = "帖子统计")
    @JsonSerialize(using = ToStringSerializer.class)
    private Integer countTopic;
    /** 今天发帖 */
    @Excel(name = "今天发帖")
    @JsonSerialize(using = ToStringSerializer.class)
    private Integer countTopicToday;
    /** 成员数量 */
    @Excel(name = "成员数量")
    @JsonSerialize(using = ToStringSerializer.class)
    private Integer countUser;
    /** 未审数量 */
    @Excel(name = "未审数量")
    @JsonSerialize(using = ToStringSerializer.class)
    private Integer countTopicAudit;
    /** 回复统计 */
    @Excel(name = "回复数量")
    @JsonSerialize(using = ToStringSerializer.class)
    private Integer countComment;
    /** 是否推荐 */
    @Excel(name = "是否推荐")
    private String isrecommend;
    /** 是否公开 */
    @Excel(name = "是否公开")
    private String isopen;
    /** 是否审核 */
    @Excel(name = "是否审核")
    private String isaudit;
    /** 允许发帖 */
    @Excel(name = "允许发帖")
    private String ispost;
    /** 是否显示 */
    @Excel(name = "是否显示")
    private String isshow;
    /** 是否审核 */
    @Excel(name = "是否审核")
    private String ispostaudit;
    /** 排序ID */
    @Excel(name = "排序ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Integer sortOrder;
    /** 审核状态 */
    @Excel(name = "审核状态")
    private String status;
    /** 创建时间 */
    @Excel(name = "添加时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
}
