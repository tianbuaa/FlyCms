package com.flycms.modules.group.service.impl;

import com.flycms.common.utils.DateUtils;

import com.flycms.common.utils.bean.BeanConvertor;
import com.flycms.modules.group.domain.Group;
import com.flycms.modules.group.domain.GroupTopic;
import com.flycms.modules.group.domain.dto.GroupTopicColumnDTO;
import com.flycms.modules.group.domain.dto.GroupTopicDTO;
import com.flycms.modules.group.service.IGroupService;
import com.flycms.modules.group.service.IGroupTopicColumnService;
import com.flycms.modules.group.service.IGroupTopicRecommendService;
import com.flycms.modules.user.domain.User;
import com.flycms.modules.user.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.constant.UserConstants;
import com.flycms.framework.security.LoginAdmin;
import com.flycms.framework.security.service.TokenService;
import com.flycms.common.utils.ServletUtils;
import com.flycms.modules.group.mapper.GroupTopicRecommendMapper;
import com.flycms.modules.group.domain.GroupTopicRecommend;
import com.flycms.modules.group.domain.dto.GroupTopicRecommendDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * 内容推荐Service业务层处理
 * 
 * @author admin
 * @date 2020-12-07
 */
@Service
public class GroupTopicRecommendServiceImpl implements IGroupTopicRecommendService
{
    @Autowired
    private GroupTopicRecommendMapper groupTopicRecommendMapper;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private IGroupService groupService;
    @Autowired
    private IUserService userService;
    @Autowired
    private IGroupTopicColumnService groupTopicColumnService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增内容推荐
     *
     * @param groupTopicRecommend 内容推荐
     * @return 结果
     */
    @Override
    public int insertGroupTopicRecommend(GroupTopicRecommend groupTopicRecommend)
    {
        groupTopicRecommend.setId(SnowFlakeUtils.nextId());
        LoginAdmin loginAdmin = tokenService.getLoginAdmin(ServletUtils.getRequest());
        Long adminId = loginAdmin.getAdmin().getAdminId();
        groupTopicRecommend.setAdminId(adminId);
        groupTopicRecommend.setCreateTime(DateUtils.getNowDate());
        return groupTopicRecommendMapper.insertGroupTopicRecommend(groupTopicRecommend);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 按内容类型和内容id删除推荐设置
     *
     * @param contentTypeId
     * @param contentId
     * @return
     */
    @Override
    public int deleteGroupTopicRecommendByContentId(int contentTypeId,Long contentId){
        return groupTopicRecommendMapper.deleteGroupTopicRecommendByContentId(contentTypeId, contentId);
    }

    /**
     * 批量删除内容推荐
     *
     * @param ids 需要删除的内容推荐ID
     * @return 结果
     */
    @Override
    public int deleteGroupTopicRecommendByIds(Long[] ids)
    {
        return groupTopicRecommendMapper.deleteGroupTopicRecommendByIds(ids);
    }

    /**
     * 删除内容推荐信息
     *
     * @param id 内容推荐ID
     * @return 结果
     */
    @Override
    public int deleteGroupTopicRecommendById(Long id)
    {
        return groupTopicRecommendMapper.deleteGroupTopicRecommendById(id);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改内容推荐
     *
     * @param groupTopicRecommend 内容推荐
     * @return 结果
     */
    @Override
    public int updateGroupTopicRecommend(GroupTopicRecommend groupTopicRecommend)
    {
        return groupTopicRecommendMapper.updateGroupTopicRecommend(groupTopicRecommend);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验内容推荐关联是否唯一
     *
     * @param groupTopicRecommend 小组话题
     * @return 结果
     */
    @Override
    public String checkGroupTopicRecommendUnique(GroupTopicRecommend groupTopicRecommend)
    {
        int count = groupTopicRecommendMapper.checkGroupTopicRecommendUnique(groupTopicRecommend);
        if (count > 0)
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 查询内容推荐
     *
     * @param contentTypeId 内容类型
     * @param groupId  小组id
     * @param contentId 内容推荐ID
     */
    @Override
    public String[] findGroupTopicRecommendById(Long contentTypeId,Long groupId,Long contentId)
    {
        List<String> recommendList = groupTopicRecommendMapper.findGroupTopicRecommendById(contentTypeId,groupId,contentId);
        String[] strArray = recommendList.toArray(new String[recommendList.size()]);
        return strArray;
    }

    /**
     * 查询内容推荐列表
     *
     * @param groupTopicRecommend 内容推荐
     * @return 内容推荐
     */
    @Override
    public Pager<GroupTopicDTO> selectGroupTopicRecommendPager(GroupTopicRecommend groupTopicRecommend, Integer page, Integer pageSize, String sort, String order)
    {
        Pager<GroupTopicDTO> pager=new Pager(page,pageSize);
        //排序设置
        pager.addOrderProperty("gt.id", true,true);
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(groupTopicRecommend);

        List<GroupTopic> groupTopicRecommendList=groupTopicRecommendMapper.selectGroupTopicRecommendPager(pager);
        List<GroupTopicDTO> dtolsit = new ArrayList<GroupTopicDTO>();
        groupTopicRecommendList.forEach(entity -> {
            GroupTopicDTO dto = new GroupTopicDTO();
            dto.setId(entity.getId());
            dto.setColumnId(entity.getColumnId());
            GroupTopicColumnDTO column=groupTopicColumnService.findGroupTopicColumnById(entity.getColumnId());
            if(column != null){
                dto.setColumnName(column.getColumnName());
            }
            Group group=groupService.findGroupById(entity.getGroupId());
            if(group != null){
                dto.setGroupName(group.getGroupName());
            }
            dto.setGroupId(entity.getGroupId());
            dto.setUserId(entity.getUserId());
            User user=userService.findUserById(entity.getUserId());
            if(user != null){
                dto.setNickname(user.getNickname());
            }
            dto.setTitle(entity.getTitle());
            dto.setContent(entity.getContent());
            dto.setCountComment(entity.getCountComment());
            dto.setCountView(entity.getCountView());
            dto.setCountDigg(entity.getCountDigg());
            dto.setCountBurys(entity.getCountBurys());
            dto.setIstop(entity.getIstop());
            dto.setIsclose(entity.getIsclose());
            dto.setIscomment(entity.getIscomment());
            dto.setIscommentshow(entity.getIscommentshow());
            dto.setIsposts(entity.getIsposts());
            dto.setIsaudit(entity.getIsaudit());
            dto.setStatus(entity.getStatus());
            dto.setCreateTime(entity.getCreateTime());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(groupTopicRecommendMapper.queryGroupTopicRecommendTotal(pager));
        return pager;
    }

    /**
     * 查询需要导出的内容推荐列表
     *
     * @param groupTopicRecommend 内容推荐
     * @return 内容推荐集合
     */
    @Override
    public List<GroupTopicRecommendDTO> exportGroupTopicRecommendList(GroupTopicRecommend groupTopicRecommend) {
        return BeanConvertor.copyList(groupTopicRecommendMapper.exportGroupTopicRecommendList(groupTopicRecommend),GroupTopicRecommendDTO.class);
    }
}
