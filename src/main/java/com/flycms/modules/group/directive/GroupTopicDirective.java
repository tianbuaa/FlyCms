package com.flycms.modules.group.directive;

import com.flycms.common.utils.StrUtils;
import com.flycms.framework.web.tag.BaseTag;
import com.flycms.modules.group.domain.*;
import com.flycms.modules.group.domain.dto.GroupTopicColumnDTO;
import com.flycms.modules.group.domain.dto.GroupTopicDTO;
import com.flycms.modules.group.service.IGroupTopicColumnService;
import com.flycms.modules.group.service.IGroupTopicCommentService;
import com.flycms.modules.group.service.IGroupTopicRecommendService;
import com.flycms.modules.group.service.IGroupTopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class GroupTopicDirective extends BaseTag {

    @Autowired
    private IGroupTopicService groupTopicService;

    @Autowired
    private IGroupTopicColumnService groupTopicColumnService;

    @Autowired
    private IGroupTopicCommentService groupTopicCommentService;

    @Autowired
    private IGroupTopicRecommendService groupTopicRecommendService;

    public GroupTopicDirective() {
        super(GroupTopicDirective.class.getName());
    }

    public Object paginate(Map params) {
        int pageNum = this.getPageNum(params);
        int pageSize = this.getPageSize(params);
        String title = getParam(params, "title");
        Long columnId = getLongParam(params, "columnId");
        Long groupId = getLongParam(params, "groupId");
        Long userId = getLongParam(params, "userId");
        String sort = getParam(params, "sort");
        String order = getParam(params, "order");
        int status = getStatusParam(params,"status");
        int isaudit = getStatusParam(params,"isaudit");
        int deleted = getDeletedTopic(params);
        GroupTopic groupTopic = new GroupTopic();
        groupTopic.setGroupId(groupId);
        groupTopic.setColumnId(columnId);
        groupTopic.setUserId(userId);
        groupTopic.setStatus(String.valueOf(status));
        groupTopic.setTitle(title);
        groupTopic.setIsaudit(String.valueOf(isaudit));
        groupTopic.setDeleted(String.valueOf(deleted));
        return groupTopicService.selectGroupTopicPager(groupTopic, pageNum, pageSize, sort, order);
    }

    public Object recommend(Map params) {
        int pageNum = this.getPageNum(params);
        int pageSize = this.getPageSize(params);
        Long infoId = getLongParam(params, "infoId");
        Long groupId = getLongParam(params, "groupId");
        String sort = getParam(params, "sort");
        String order = getParam(params, "order");
        int type = getParamInt(params,"type");
        int tj = getParamInt(params,"tj");
        GroupTopicRecommend topicRecommend = new GroupTopicRecommend();
        topicRecommend.setGroupId(groupId);
        topicRecommend.setContentTypeId(type);
        topicRecommend.setContentId(infoId);
        topicRecommend.setRecommend(tj);
        return groupTopicRecommendService.selectGroupTopicRecommendPager(topicRecommend, pageNum, pageSize, sort, order);
    }

    public Object classify(Map params) {
        String title = getParam(params, "title");
        Long columnId = getLongParam(params, "columnId");
        Long groupId = getLongParam(params, "groupId");
        int status = getStatusParam(params,"status");
        GroupTopicColumn groupTopicColumn = new GroupTopicColumn();
        if(!StrUtils.isEmpty(title) && columnId == null && groupId==null){
            return null;
        }
        groupTopicColumn.setGroupId(groupId);
        groupTopicColumn.setColumnName(title);
        groupTopicColumn.setStatus(status);
        return groupTopicColumnService.selectGroupTopicColumnList(groupTopicColumn);
    }

    /**
     * 小组分类详细信息
     *
     * @param params
     * @return
     */
    public Object columninfo(Map params) {
        Long id = getLongParam(params, "id");
        GroupTopicColumnDTO column = null;
        if(id != null){
            column = groupTopicColumnService.findGroupTopicColumnById(id);
        }
        return column;
    }

    /**
     * 小组详细信息
     *
     * @param params
     * @return
     */
    public Object topicInfo(Map params) {
        Long id = getLongParam(params, "id");
        GroupTopicDTO topic = null;
        if(id != null){
            topic = groupTopicService.findGroupTopicById(id);
        }
        return topic;
    }

    public Object comment(Map params) {
        int pageNum = this.getPageNum(params);
        int pageSize = this.getPageSize(params);
        Long userId = getLongParam(params, "userId");
        Long topicId = getLongParam(params, "topicId");
        Long referId = getLongParam(params, "referId");
        String sort = getParam(params, "sort");
        String order = getParam(params, "order");
        int status = getStatusParam(params,"status");
        int deleted = getDeletedTopic(params);
        GroupTopicComment topicComment = new GroupTopicComment();
        if(referId == null && userId == null && topicId==null){
            return null;
        }
        topicComment.setUserId(userId);
        topicComment.setTopicId(topicId);
        topicComment.setReferId(referId);
        topicComment.setStatus(status);
        topicComment.setDeleted(deleted);
        return groupTopicCommentService.selectGroupTopicCommentPager(topicComment, pageNum, pageSize, sort, order);
    }

    public Object reComment(Map params) {
        int pageNum = this.getPageNum(params);
        int pageSize = this.getPageSize(params);
        Long userId = getLongParam(params, "userId");
        Long topicId = getLongParam(params, "topicId");
        Long referId = getLongParam(params, "referId");
        String sort = getParam(params, "sort");
        String order = getParam(params, "order");
        int status = getStatusParam(params,"status");
        int deleted = getDeletedTopic(params);
        GroupTopicComment topicComment = new GroupTopicComment();
        if(referId == null && userId == null && topicId==null){
            return null;
        }
        topicComment.setUserId(userId);
        topicComment.setTopicId(topicId);
        topicComment.setReferId(referId);
        topicComment.setStatus(status);
        topicComment.setDeleted(deleted);
        return groupTopicCommentService.selectGroupTopicCommentPager(topicComment, pageNum, pageSize, sort, order);
    }

    /**
     * 随机标签列表
     *
     * @param params
     * @return
     */
    public Object randList(Map params) {
        int pageSize = getParamInt(params, "pageSize");
        return groupTopicService.selectRandTopicList(pageSize);
    }

}
