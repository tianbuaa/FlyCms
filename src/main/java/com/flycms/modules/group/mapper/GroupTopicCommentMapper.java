package com.flycms.modules.group.mapper;

import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.group.domain.GroupTopic;
import com.flycms.modules.group.domain.GroupTopicComment;
import org.springframework.stereotype.Repository;

/**
 * 话题回复/评论Mapper接口
 * 
 * @author admin
 * @date 2020-12-15
 */
@Repository
public interface GroupTopicCommentMapper
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增话题回复/评论
     *
     * @param groupTopicComment 话题回复/评论
     * @return 结果
     */
    public int insertGroupTopicComment(GroupTopicComment groupTopicComment);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除话题回复/评论
     *
     * @param id 话题回复/评论ID
     * @return 结果
     */
    public int deleteGroupTopicCommentById(Long id);

    /**
     * 批量删除话题回复/评论
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteGroupTopicCommentByIds(Long[] ids);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改话题回复/评论
     *
     * @param groupTopicComment 话题回复/评论
     * @return 结果
     */
    public int updateGroupTopicComment(GroupTopicComment groupTopicComment);

    /**
     * 更新评论话题数量
     *
     * @param id 话题回复/评论ID
     * @return 结果
     */
    public int updateCountComment(int countComment,Long id);
    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验话题回复/评论是否唯一
     *
     * @param groupTopicComment 话题回复/评论
     * @return 结果
     */
    public int checkGroupTopicCommentUnique(GroupTopicComment groupTopicComment);

    /**
     * 按话题ID查询最新话题回复信息
     *
     * @param topicId 话题ID
     * @return
     */
    public GroupTopicComment findNewestGroupTopicComment(Long topicId);

    /**
     * 查询话题回复/评论
     * 
     * @param id 话题回复/评论ID
     * @return 话题回复/评论
     */
    public GroupTopicComment findGroupTopicCommentById(Long id);

    /**
     * 查询话题评论数量（盖楼数量）
     *
     * @param topicId 话题id
     * @return
     */
    public int queryTopicCommentCount(Long topicId);

    /**
     * 查询评论回复统计
     *
     * @param referId 上级评论ID
     * @return
     */
    public int queryReCommentCount(Long referId);

    /**
     * 查询话题回复/评论数量
     *
     * @param pager 分页处理类
     * @return 话题回复/评论数量
     */
    public int queryGroupTopicCommentTotal(Pager pager);

    /**
     * 查询话题回复/评论列表
     * 
     * @param pager 分页处理类
     * @return 话题回复/评论集合
     */
    public List<GroupTopicComment> selectGroupTopicCommentPager(Pager pager);

    /**
     * 查询需要导出的话题回复/评论列表
     *
     * @param groupTopicComment 话题回复/评论
     * @return 话题回复/评论集合
     */
    public List<GroupTopicComment> exportGroupTopicCommentList(GroupTopicComment groupTopicComment);
}
