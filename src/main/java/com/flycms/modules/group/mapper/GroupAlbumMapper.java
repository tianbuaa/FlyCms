package com.flycms.modules.group.mapper;

import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.group.domain.GroupAlbum;
import org.springframework.stereotype.Repository;

/**
 * 小组帖子专辑Mapper接口
 * 
 * @author admin
 * @date 2020-12-16
 */
@Repository
public interface GroupAlbumMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增小组帖子专辑
     *
     * @param groupAlbum 小组帖子专辑
     * @return 结果
     */
    public int insertGroupAlbum(GroupAlbum groupAlbum);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除小组帖子专辑
     *
     * @param id 小组帖子专辑ID
     * @return 结果
     */
    public int deleteGroupAlbumById(Long id);

    /**
     * 批量删除小组帖子专辑
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteGroupAlbumByIds(Long[] ids);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改小组帖子专辑
     *
     * @param groupAlbum 小组帖子专辑
     * @return 结果
     */
    public int updateGroupAlbum(GroupAlbum groupAlbum);


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验专辑名字是否唯一
     *
     * @param groupAlbum 小组帖子专辑ID
     * @return 结果
     */
    public int checkGroupAlbumAlbumNameUnique(GroupAlbum  groupAlbum);


    /**
     * 查询小组帖子专辑
     * 
     * @param id 小组帖子专辑ID
     * @return 小组帖子专辑
     */
    public GroupAlbum findGroupAlbumById(Long id);

    /**
     * 查询小组帖子专辑数量
     *
     * @param pager 分页处理类
     * @return 小组帖子专辑数量
     */
    public int queryGroupAlbumTotal(Pager pager);

    /**
     * 查询小组帖子专辑列表
     * 
     * @param pager 分页处理类
     * @return 小组帖子专辑集合
     */
    public List<GroupAlbum> selectGroupAlbumPager(Pager pager);

    /**
     * 查询需要导出的小组帖子专辑列表
     *
     * @param groupAlbum 小组帖子专辑
     * @return 小组帖子专辑集合
     */
    public List<GroupAlbum> exportGroupAlbumList(GroupAlbum groupAlbum);
}
