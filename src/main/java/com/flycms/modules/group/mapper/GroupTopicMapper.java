package com.flycms.modules.group.mapper;

import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.group.domain.Group;
import com.flycms.modules.group.domain.GroupTopic;
import org.springframework.stereotype.Repository;

/**
 * 小组话题Mapper接口
 * 
 * @author admin
 * @date 2020-09-27
 */
@Repository
public interface GroupTopicMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增小组话题
     *
     * @param groupTopic 小组话题
     * @return 结果
     */
    public int insertGroupTopic(GroupTopic groupTopic);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除小组话题
     *
     * @param id 小组话题ID
     * @return 结果
     */
    public int deleteGroupTopicById(Long id);

    /**
     * 批量删除小组话题
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteGroupTopicByIds(Long[] ids);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改小组话题
     *
     * @param groupTopic 小组话题
     * @return 结果
     */
    public int updateGroupTopic(GroupTopic groupTopic);

    /**
     * 修改小组话题假删除
     *
     * @param id 小组话题ID
     * @return 结果
     */
    public int updateDeleteGroupTopicById(Long id);

    /**
     * 更新话题评论数量
     *
     * @param id
     * @return
     */
    public int updateCountComment(Long id);

    /**
     * 更新用户话题数量
     *
     * @param userId 用户id
     * @return
     */
    public int updateUserCountTopic(Long userId);

    /**
     * 更新话题被关注数量
     *
     * @param id 话题ID
     * @return
     */
    public int updateTopicFollowCount(Long id);

    /**
     * 更新话题浏览数量
     *
     * @param id 小组话题ID
     * @return
     */
    public int updateCountView(Long id);

    /**
     * 更新话题排序权重
     *
     * @param weight 权重值
     * @param id  小组话题ID
     * @return
     */
    public int updateWeight(Double weight,Long id);
    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验群组小组话题是否唯一
     *
     * @param groupTopic 小组话题
     * @return 结果
     */
    public int checkGroupTopicNameUnique(GroupTopic groupTopic);

    /**
     * 查询小组话题
     * 
     * @param id 小组话题ID
     * @return 小组话题
     */
    public GroupTopic findGroupTopicById(Long id);

    /**
     * 按ID查询内容的上一页
     *
     * @param groupTopic 小组话题
     * @return 小组话题ID和标题
     */
    public GroupTopic findGroupTopicByPrePage(GroupTopic groupTopic);

    /**
     * 按ID查询内容的下一页
     *
     * @param groupTopic 小组话题
     * @return 小组话题ID和标题
     */
    public GroupTopic findGroupTopicByNextPage(GroupTopic groupTopic);

    /**
     * 查询小组话题数量
     *
     * @param pager 分页处理类
     * @return 小组话题数量
     */
    public int queryGroupTopicTotal(Pager pager);

    /**
     * 查询小组话题列表
     * 
     * @param pager 分页处理类
     * @return 小组话题集合
     */
    public List<GroupTopic> selectGroupTopicPager(Pager pager);

    /**
     * 查询小组话题所有列表
     *
     * @param total 查询数量
     * @return
     */
    public List<GroupTopic> selectRandTopicList(int total);

    /**
     * 查询小组话题所有列表
     *
     * @return
     */
    public List<GroupTopic> selectGroupTopicAllList();

}
