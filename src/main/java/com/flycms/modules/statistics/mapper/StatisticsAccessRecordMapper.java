package com.flycms.modules.statistics.mapper;

import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.statistics.domain.StatisticsAccessRecord;
import org.springframework.stereotype.Repository;

/**
 * 访问记录Mapper接口
 * 
 * @author admin
 * @date 2020-12-09
 */
@Repository
public interface StatisticsAccessRecordMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增访问记录
     *
     * @param statisticsAccessRecord 访问记录
     * @return 结果
     */
    public int insertStatisticsAccessRecord(StatisticsAccessRecord statisticsAccessRecord);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除访问记录
     *
     * @param id 访问记录ID
     * @return 结果
     */
    public int deleteStatisticsAccessRecordById(Long id);

    /**
     * 批量删除访问记录
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStatisticsAccessRecordByIds(Long[] ids);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改访问记录
     *
     * @param statisticsAccessRecord 访问记录
     * @return 结果
     */
    public int updateStatisticsAccessRecord(StatisticsAccessRecord statisticsAccessRecord);


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询访问记录
     * 
     * @param id 访问记录ID
     * @return 访问记录
     */
    public StatisticsAccessRecord findStatisticsAccessRecordById(Long id);

    /**
     * 查询访问记录数量
     *
     * @param pager 分页处理类
     * @return 访问记录数量
     */
    public int queryStatisticsAccessRecordTotal(Pager pager);

    /**
     * 查询访问记录列表
     * 
     * @param pager 分页处理类
     * @return 访问记录集合
     */
    public List<StatisticsAccessRecord> selectStatisticsAccessRecordPager(Pager pager);

    /**
     * 查询是否是老访客
     *
     * @param cookieId  客户端cookie判断是否以前访问过
     * @return
     */
    public int queryStatisticsAccessRecordCookieIdCount(String cookieId);

    /**
     * 查询访问记录总数
     *
     * @param time
     * @return
     */
    public int queryStatisticsAccessRecordCount(int time);

    /**
     * 查询需要导出的访问记录列表
     *
     * @param time 查询时间类型，1今天，2昨天，3本周，4本月，5上个月，6本年
     * @param offset 起始数
     * @param rows   结束数
     * @return 访问记录集合
     */
    public List<StatisticsAccessRecord> selectStatisticsAccessRecordList(Integer time,Integer offset, Integer rows);
}
