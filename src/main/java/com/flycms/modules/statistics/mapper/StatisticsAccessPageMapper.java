package com.flycms.modules.statistics.mapper;

import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.statistics.domain.StatisticsAccessPage;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 受访页面日报Mapper接口
 * 
 * @author admin
 * @date 2021-01-12
 */
@Repository
public interface StatisticsAccessPageMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增受访页面日报
     *
     * @param statisticsAccessPage 受访页面日报
     * @return 结果
     */
    public int insertStatisticsAccessPage(StatisticsAccessPage statisticsAccessPage);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除受访页面日报
     *
     * @param id 受访页面日报ID
     * @return 结果
     */
    public int deleteStatisticsAccessPageById(Long id);

    /**
     * 批量删除受访页面日报
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStatisticsAccessPageByIds(Long[] ids);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改受访页面日报
     *
     * @param statisticsAccessPage 受访页面日报
     * @return 结果
     */
    public int updateStatisticsAccessPage(StatisticsAccessPage statisticsAccessPage);


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 校验查询受访页面是否唯一
     *
     * @param url
     * @param time
     * @return
     */
    public int checkAccessPageNameUnique(String url, int time);
    /**
     * 查询受访页面日报
     * 
     * @param id 受访页面日报ID
     * @return 受访页面日报
     */
    public StatisticsAccessPage findStatisticsAccessPageById(Long id);

    /**
     * 按时间筛选页面被访问数量
     *
     * @param time
     * @return
     */
    public int queryStatisticsAccessPageCount(Integer time);

    /**
     * 查询受访页面日报数量
     *
     * @param pager 分页处理类
     * @return 受访页面日报数量
     */
    public int queryStatisticsAccessPageTotal(Pager pager);

    /**
     * 查询受访页面日报列表
     * 
     * @param pager 分页处理类
     * @return 受访页面日报集合
     */
    public List<StatisticsAccessPage> selectStatisticsAccessPagePager(Pager pager);

    /**
     * 查询需要导出的受访页面日报列表
     *
     * @param statisticsAccessPage 受访页面日报
     * @return 受访页面日报集合
     */
    public List<StatisticsAccessPage> exportStatisticsAccessPageList(StatisticsAccessPage statisticsAccessPage);

    /**
     * 受访页面列表
     *
     * @param time 查询时间类型，1今天，2昨天，3本周，4本月，5上个月，6本年
     * @param offset 起始数
     * @param rows   结束数
     * @return
     */
    public List<StatisticsAccessPage> getAccessPageList(Integer time,Integer offset, Integer rows);
}
