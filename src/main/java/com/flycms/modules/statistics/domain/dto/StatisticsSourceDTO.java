package com.flycms.modules.statistics.domain.dto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

/**
 * 来源统计数据传输对象 fly_statistics_source
 * 
 * @author admin
 * @date 2020-12-09
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class StatisticsSourceDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** id */
    @Excel(name = "id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** 统计日期（格式:yyyy-MM-dd） */
    @Excel(name = "统计日期", readConverterExp = "格=式:yyyy-MM-dd")
    private String statisticsDay;
    /** 来源网站类型 （1-搜索引擎  2-外部链接  3-直接访问） */
    @Excel(name = "来源网站类型 ", readConverterExp = "1=-搜索引擎,2=-外部链接,3=-直接访问")
    private Integer sorceUrlType;
    /** 是否新客户 （0-否   1-是） */
    @Excel(name = "是否新客户 ", readConverterExp = "0=-否,1=-是")
    private Integer isNewVisitor;
    /** 访客设备类型（1-计算机   2-移动设备） */
    @Excel(name = "访客设备类型", readConverterExp = "1=-计算机,2=-移动设备")
    private Integer visitorDeviceType;
    /** 来源域名 */
    @Excel(name = "来源域名")
    private String sourceDomain;
    /** 来源外部链接网站地址或网站名称（如：百度/http://www.jeecms.com） */
    @Excel(name = "来源外部链接网站地址或网站名称", readConverterExp = "如=：百度/http://www.jeecms.com")
    private String sorceUrl;
    /** 搜索引擎 */
    @Excel(name = "搜索引擎")
    private String engineName;
    /** 浏览量 */
    @Excel(name = "浏览量")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long pvs;
    /** 访客数 */
    @Excel(name = "访客数")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long uvs;
    /** ip数 */
    @Excel(name = "ip数")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long ips;
    /** 总访问时长(单位：秒) */
    @Excel(name = "总访问时长(单位：秒)")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long accessHoureLong;
    /** 只访问一次页面的访问次数 */
    @Excel(name = "只访问一次页面的访问次数")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long onlyOnePv;
    /** 时间段 */
    @Excel(name = "时间段")
    private Integer statisticsHour;
    /** 删除标识 */
    @Excel(name = "删除标识")
    private Integer deleted;

}
