package com.flycms.modules.site.mapper;

import java.util.List;

import com.flycms.modules.site.domain.SiteLinks;
import com.flycms.common.utils.page.Pager;
import org.springframework.stereotype.Repository;

/**
 * 友情链接Mapper接口
 * 
 * @author admin
 * @date 2020-07-08
 */
@Repository
public interface SiteLinksMapper 
{
    /////////////////////////////////
    ////////       增加       ////////
    /////////////////////////////////
    /**
     * 新增友情链接
     *
     * @param siteLinks 友情链接
     * @return 结果
     */
    public int insertSiteLinks(SiteLinks siteLinks);

    /////////////////////////////////
    ////////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除友情链接
     *
     * @param id 友情链接ID
     * @return 结果
     */
    public int deleteSiteLinksById(Long id);

    /**
     * 批量删除友情链接
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSiteLinksByIds(Long[] ids);

    /////////////////////////////////
    ////////        修改      ////////
    /////////////////////////////////
    /**
     * 修改友情链接
     *
     * @param siteLinks 友情链接
     * @return 结果
     */
    public int updateSiteLinks(SiteLinks siteLinks);


    /////////////////////////////////
    ////////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验网站名称是否唯一
     *
     * @param siteLinks 友情链接ID
     * @return 结果
     */
    public int checkSiteLinksLinkNameUnique(SiteLinks  siteLinks);


    /**
     * 查询友情链接
     * 
     * @param id 友情链接ID
     * @return 友情链接
     */
    public SiteLinks selectSiteLinksById(Long id);

    /**
     * 查询友情链接数量
     *
     * @param pager 分页处理类
     * @return 友情链接数量
     */
    public int querySiteLinksTotal(Pager pager);

    /**
     * 查询友情链接列表
     * 
     * @param pager 分页处理类
     * @return 友情链接集合
     */
    public List<SiteLinks> selectSiteLinksPager(Pager pager);

}
