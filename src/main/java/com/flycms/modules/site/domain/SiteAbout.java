package com.flycms.modules.site.domain;

import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 网站相关介绍对象 fly_site_about
 * 
 * @author admin
 * @date 2021-01-27
 */
@Data
public class SiteAbout extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;
    /** 分类id */
    private Long classifyId;
    /** 标题 */
    private String title;
    /** 内容 */
    private String content;
    /** 是否显示 */
    private Integer status;
}
