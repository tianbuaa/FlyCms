package com.flycms.modules.site.domain.dto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

/**
 * 网站相关介绍数据传输对象 fly_site_about
 * 
 * @author admin
 * @date 2021-01-27
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class SiteAboutDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @Excel(name = "ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** 分类id */
    @Excel(name = "分类id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long classifyId;
    /** 标题 */
    @Excel(name = "标题")
    private String title;
    /** 内容 */
    @Excel(name = "内容")
    private String content;
    /** 是否显示 */
    @Excel(name = "是否显示")
    private Integer status;

}
