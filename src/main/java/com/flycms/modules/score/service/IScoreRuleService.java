package com.flycms.modules.score.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.score.domain.ScoreRule;
import com.flycms.modules.score.domain.dto.ScoreRuleDTO;

import java.util.List;

public interface IScoreRuleService {
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增积分规则
     *
     * @param scoreRule 积分规则
     * @return 结果
     */
    public int insertScoreRule(ScoreRule scoreRule);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除积分规则
     *
     * @param ids 需要删除的积分规则ID
     * @return 结果
     */
    public int deleteScoreRuleByIds(Long[] ids);

    /**
     * 删除积分规则信息
     *
     * @param id 积分规则ID
     * @return 结果
     */
    public int deleteScoreRuleById(Long id);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改积分规则
     *
     * @param scoreRule 积分规则
     * @return 结果
     */
    public int updateScoreRule(ScoreRule scoreRule);

    /**
     * 按id查询规则
     *
     * @param status 规则状态
     * @param id 规则ID
     * @return
     */
    public int updateScoreRuleEnabled(String status, Long id);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 根据规则名称查找
     *
     * @param name 规则名称
     * @return
     */
    public ScoreRule findScoreRuleByName(String name);

    /**
     * 根据规则id查找和审核状态查询
     *
     * @param id
     * @return
     */
    public ScoreRule findScoreRuleById(Long id);

    /**
     * 根据积分规则奖励
     * @param userId
     *        用户id
     * @param scoreRuleId
     *        积分规则id
     * @param foreignId
     *        奖励的信息编号id
     */
    public void scoreRuleBonus(Long userId, Long scoreRuleId, Long foreignId);
    /**
     * 查询积分规则列表
     *
     * @param scoreRule 积分规则
     * @return 积分规则
     */
    public Pager<ScoreRuleDTO> selectScoreRulePager(ScoreRule scoreRule, Integer page, Integer limit, String sort, String order);

    /**
     * 查询需要导出的积分规则列表
     *
     * @param scoreRule 积分规则
     * @return 积分规则集合
     */
    public List<ScoreRuleDTO> exportScoreRuleList(ScoreRule scoreRule);

}
