package com.flycms.modules.applet.service;

import com.flycms.modules.applet.domain.AppConfig;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.applet.domain.dto.AppConfigDto;

/**
 * 小程序开发者ID设置Service接口
 * 
 * @author admin
 * @date 2020-05-27
 */
public interface IAppConfigService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增小程序开发者ID设置
     *
     * @param appConfig 小程序开发者ID设置
     * @return 结果
     */
    public int insertAppConfig(AppConfig appConfig);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除小程序开发者ID设置
     *
     * @param ids 需要删除的小程序开发者ID设置ID
     * @return 结果
     */
    public int deleteAppConfigByIds(Long[] ids);

    /**
     * 删除小程序开发者ID设置信息
     *
     * @param id 小程序开发者ID设置ID
     * @return 结果
     */
    public int deleteAppConfigById(Long id);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改小程序开发者ID设置
     *
     * @param appConfig 小程序开发者ID设置
     * @return 结果
     */
    public int updateAppConfig(AppConfig appConfig);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验平台名称和AppId是否唯一，更新时判断非当前id下是否时唯一
     *
     * @param appConfig platform平台名称、appId开发者ID，！id
     * @return 结果
     */
    public String checkAppConfigUnique(AppConfig appConfig);

    /**
     * 查询小程序开发者ID设置
     * 
     * @param id 小程序开发者ID设置ID
     * @return 小程序开发者ID设置
     */
    public AppConfig selectAppConfigById(Long id);

    /**
     * 查询小程序开发者ID设置列表
     * 
     * @param appConfig 小程序开发者ID设置
     * @return 小程序开发者ID设置集合
     */
    public Pager<AppConfigDto> selectAppConfigPager(AppConfig appConfig, Integer page, Integer limit, String sort, String order);
}
