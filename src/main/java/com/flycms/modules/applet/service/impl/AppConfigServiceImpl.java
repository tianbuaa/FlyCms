package com.flycms.modules.applet.service.impl;

import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.DateUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.applet.domain.AppConfig;
import com.flycms.modules.applet.domain.dto.AppConfigDto;
import com.flycms.modules.applet.mapper.AppConfigMapper;
import com.flycms.modules.applet.service.IAppConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 小程序开发者ID设置Service业务层处理
 * 
 * @author admin
 * @date 2020-05-27
 */
@Service
public class AppConfigServiceImpl implements IAppConfigService
{
    @Autowired
    private AppConfigMapper appConfigMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增小程序开发者ID设置
     *
     * @param appConfig 小程序开发者ID设置
     * @return 结果
     */
    @Override
    public int insertAppConfig(AppConfig appConfig)
    {
        appConfig.setId(SnowFlakeUtils.nextId());
        appConfig.setCreateTime(DateUtils.getNowDate());
        return appConfigMapper.insertAppConfig(appConfig);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除小程序开发者ID设置
     *
     * @param ids 需要删除的小程序开发者ID设置ID
     * @return 结果
     */
    @Override
    public int deleteAppConfigByIds(Long[] ids)
    {
        return appConfigMapper.deleteAppConfigByIds(ids);
    }

    /**
     * 删除小程序开发者ID设置信息
     *
     * @param id 小程序开发者ID设置ID
     * @return 结果
     */
    @Override
    public int deleteAppConfigById(Long id)
    {
        return appConfigMapper.deleteAppConfigById(id);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改小程序开发者ID设置
     *
     * @param appConfig 小程序开发者ID设置
     * @return 结果
     */
    @Override
    public int updateAppConfig(AppConfig appConfig)
    {
        return appConfigMapper.updateAppConfig(appConfig);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验平台名称和AppId是否唯一，更新时判断非当前id下是否时唯一
     *
     * @param appConfig platform平台名称、appId开发者ID，！id
     * @return 结果
     */
     @Override
     public String checkAppConfigUnique(AppConfig appConfig)
     {
         int count = appConfigMapper.checkAppConfigUnique(appConfig);
          if (count > 0)
          {
             return UserConstants.NOT_UNIQUE;
           }
           return UserConstants.UNIQUE;
     }

    /**
     * 查询小程序开发者ID设置
     * 
     * @param id 小程序开发者ID设置ID
     * @return 小程序开发者ID设置
     */
    @Override
    @Cacheable(cacheNames = "appConfig",key = "'appConfig_'+#id",sync=true)
    public AppConfig selectAppConfigById(Long id)
    {
        return appConfigMapper.selectAppConfigById(id);
    }


    /**
     * 查询小程序开发者ID设置列表
     *
     * @param appConfig 小程序开发者ID设置
     * @return 小程序开发者ID设置
     */
    @Override
    public Pager<AppConfigDto> selectAppConfigPager(AppConfig appConfig, Integer page, Integer pageSize, String sort, String order)
    {
        Pager<AppConfigDto> pager=new Pager(page,pageSize);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(appConfig);

        List<AppConfig> appConfigList=appConfigMapper.selectAppConfigPager(pager);
        List<AppConfigDto> dtolsit = new ArrayList<AppConfigDto>();
        appConfigList.forEach(appConfigs -> {
            AppConfigDto appConfigDto = new AppConfigDto();
            appConfigDto.setId(appConfigs.getId());
            appConfigDto.setPlatform(appConfigs.getPlatform());
            appConfigDto.setAppId(appConfigs.getAppId());
            appConfigDto.setPlatformUrl(appConfigs.getPlatformUrl());
            dtolsit.add(appConfigDto);
        });
        pager.setList(dtolsit);
        pager.setTotal(appConfigMapper.queryAppConfigTotal(pager));
        return pager;
    }

}
