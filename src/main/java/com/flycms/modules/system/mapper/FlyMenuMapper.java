package com.flycms.modules.system.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.flycms.modules.system.domain.FlyMenu;
import org.springframework.stereotype.Repository;

/**
 * 菜单表 数据层
 * 
 * @author kaifei sun
 */
@Repository
public interface FlyMenuMapper
{
    /////////////////////////////////
    ////////       增加       ////////
    /////////////////////////////////
    /**
     * 新增菜单信息
     *
     * @param menu 菜单信息
     * @return 结果
     */
    public int insertMenu(FlyMenu menu);
    /////////////////////////////////
    ////////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除菜单管理信息
     *
     * @param menuId 菜单ID
     * @return 结果
     */
    public int deleteMenuById(Long menuId);
    /////////////////////////////////
    ////////        修改      ////////
    /////////////////////////////////
    /**
     * 修改菜单信息
     *
     * @param menu 菜单信息
     * @return 结果
     */
    public int updateMenu(FlyMenu menu);
    /////////////////////////////////
    ////////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询系统菜单列表
     * 
     * @param menu 菜单信息
     * @return 菜单列表
     */
    public List<FlyMenu> selectMenuList(FlyMenu menu);

    /**
     * 根据用户所有权限
     * 
     * @return 权限列表
     */
    public List<String> selectMenuPerms();

    /**
     * 根据用户查询系统菜单列表
     * 
     * @param menu 菜单信息
     * @return 菜单列表
     */
    public List<FlyMenu> selectMenuListByAdminId(FlyMenu menu);

    /**
     * 根据用户ID查询权限
     * 
     * @param adminId 用户ID
     * @return 权限列表
     */
    public List<String> selectMenuPermsByAdminId(Long adminId);

    /**
     * 根据用户ID查询菜单
     * 
     * @return 菜单列表
     */
    public List<FlyMenu> selectMenuTreeAll();

    /**
     * 根据用户ID查询菜单
     * 
     * @param adminId 用户ID
     * @return 菜单列表
     */
    public List<FlyMenu> selectMenuTreeByAdminId(Long adminId);

    /**
     * 根据角色ID查询菜单树信息
     * 
     * @param roleId 角色ID
     * @return 选中菜单列表
     */
    public List<Integer> selectMenuListByRoleId(Long roleId);

    /**
     * 根据菜单ID查询信息
     * 
     * @param menuId 菜单ID
     * @return 菜单信息
     */
    public FlyMenu selectMenuById(Long menuId);

    /**
     * 是否存在菜单子节点
     * 
     * @param menuId 菜单ID
     * @return 结果
     */
    public int hasChildByMenuId(Long menuId);

    /**
     * 校验菜单名称是否唯一
     * 
     * @param menuName 菜单名称
     * @param parentId 父菜单ID
     * @return 结果
     */
    public FlyMenu checkMenuNameUnique(@Param("menuName") String menuName, @Param("parentId") Long parentId);
}
