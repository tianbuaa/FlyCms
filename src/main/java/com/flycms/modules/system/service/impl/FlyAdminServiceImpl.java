package com.flycms.modules.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.flycms.common.constant.UserConstants;
import com.flycms.common.exception.CustomException;
import com.flycms.common.utils.SecurityUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.framework.aspectj.lang.annotation.DataScope;
import com.flycms.modules.system.domain.*;
import com.flycms.modules.system.domain.dto.AdminQueryDTO;
import com.flycms.modules.system.mapper.*;
import com.flycms.modules.system.service.IFlyAdminService;
import com.flycms.modules.system.service.IFlyConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 用户 业务层处理
 * 
 * @author kaifei sun
 */
@Service
public class FlyAdminServiceImpl implements IFlyAdminService
{
    private static final Logger log = LoggerFactory.getLogger(FlyAdminServiceImpl.class);

    @Autowired
    private FlyAdminMapper adminMapper;

    @Autowired
    private FlyRoleMapper roleMapper;

    @Autowired
    private FlyDeptMapper deptMapper;

    @Autowired
    private FlyPostMapper postMapper;

    @Autowired
    private FlyAdminRoleMapper adminRoleMapper;

    @Autowired
    private FlyAdminPostMapper userPostMapper;

    @Autowired
    private IFlyConfigService configService;

    /////////////////////////////////
    ///////        增加       ////////
    /////////////////////////////////
    /**
     * 新增保存用户信息
     *
     * @param admin 用户信息
     * @return 结果
     */
    @Override
    @Transactional
    public int insertAdmin(FlyAdmin admin)
    {
        // 新增用户信息
        admin.setAdminId(SnowFlakeUtils.nextId());
        int rows = adminMapper.insertAdmin(admin);
        // 新增用户岗位关联
        insertAdminPost(admin);
        // 新增用户与角色管理
        insertAdminRole(admin);
        return rows;
    }

    /**
     * 新增用户角色信息
     *
     * @param admin 用户对象
     */
    public void insertAdminRole(FlyAdmin admin)
    {
        Long[] roles = admin.getRoleIds();
        if (StrUtils.isNotNull(roles))
        {
            // 新增用户与角色管理
            List<FlyAdminRole> list = new ArrayList<FlyAdminRole>();
            for (Long roleId : roles)
            {
                FlyAdminRole ur = new FlyAdminRole();
                ur.setAdminId(admin.getAdminId());
                ur.setRoleId(roleId);
                list.add(ur);
            }
            if (list.size() > 0)
            {
                adminRoleMapper.batchAdminRole(list);
            }
        }
    }

    /**
     * 新增用户岗位信息
     *
     * @param admin 用户对象
     */
    public void insertAdminPost(FlyAdmin admin)
    {
        Long[] posts = admin.getPostIds();
        if (StrUtils.isNotNull(posts))
        {
            // 新增用户与岗位管理
            List<FlyAdminPost> list = new ArrayList<FlyAdminPost>();
            for (Long postId : posts)
            {
                FlyAdminPost up = new FlyAdminPost();
                up.setAdminId(admin.getAdminId());
                up.setPostId(postId);
                list.add(up);
            }
            if (list.size() > 0)
            {
                userPostMapper.batchAdminPost(list);
            }
        }
    }



    /**
     * 导入用户数据
     *
     * @param adminList 用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importAdmin(List<FlyAdmin> adminList, Boolean isUpdateSupport, String operName)
    {
        if (StrUtils.isNull(adminList) || adminList.size() == 0)
        {
            throw new CustomException("导入用户数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        String password = configService.selectConfigByKey("sys.admin.initPassword");
        for (FlyAdmin user : adminList)
        {
            try
            {
                // 验证是否存在这个用户
                FlyAdmin u = adminMapper.selectAdminByName(user.getAdminName());
                if (StrUtils.isNull(u))
                {
                    user.setAdminId(SnowFlakeUtils.nextId());
                    user.setPassword(SecurityUtils.encryptPassword(password));
                    user.setCreateBy(operName);
                    this.insertAdmin(user);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、账号 " + user.getAdminName() + " 导入成功");
                }
                else if (isUpdateSupport)
                {
                    user.setUpdateBy(operName);
                    this.updateAdmin(user);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、账号 " + user.getAdminName() + " 更新成功");
                }
                else
                {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、账号 " + user.getAdminName() + " 已存在");
                }
            }
            catch (Exception e)
            {
                failureNum++;
                String msg = "<br/>" + failureNum + "、账号 " + user.getAdminName() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0)
        {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new CustomException(failureMsg.toString());
        }
        else
        {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }
    /////////////////////////////////
    ///////        刪除       ////////
    /////////////////////////////////
    /**
     * 通过用户ID删除用户
     *
     * @param adminId 用户ID
     * @return 结果
     */
    @Override
    public int deleteAdminById(Long adminId)
    {
        // 删除用户与角色关联
        adminRoleMapper.deleteAdminRoleByAdminId(adminId);
        // 删除用户与岗位表
        userPostMapper.deleteAdminPostByAdminId(adminId);
        return adminMapper.deleteAdminById(adminId);
    }

    /**
     * 批量删除用户信息
     *
     * @param adminIds 需要删除的用户ID
     * @return 结果
     */
    public int deleteAdminByIds(Long[] adminIds)
    {
        for (Long adminId : adminIds)
        {
            checkAdminAllowed(new FlyAdmin(adminId));
        }
        return adminMapper.deleteAdminByIds(adminIds);
    }

    /////////////////////////////////
    ///////        修改       ////////
    /////////////////////////////////

    /**
     * 修改保存用户信息
     *
     * @param admin 用户信息
     * @return 结果
     */
    @Override
    @Transactional
    public int updateAdmin(FlyAdmin admin)
    {
        Long adminId = admin.getAdminId();
        // 删除用户与角色关联
        adminRoleMapper.deleteAdminRoleByAdminId(adminId);
        // 新增用户与角色管理
        insertAdminRole(admin);
        // 删除用户与岗位关联
        userPostMapper.deleteAdminPostByAdminId(adminId);
        // 新增用户与岗位管理
        insertAdminPost(admin);
        return adminMapper.updateAdmin(admin);
    }

    /**
     * 修改用户状态
     *
     * @param admin 用户信息
     * @return 结果
     */
    @Override
    public int updateAdminStatus(FlyAdmin admin)
    {
        return adminMapper.updateAdmin(admin);
    }

    /**
     * 修改用户基本信息
     *
     * @param admin 用户信息
     * @return 结果
     */
    @Override
    public int updateAdminProfile(FlyAdmin admin)
    {
        return adminMapper.updateAdmin(admin);
    }

    /**
     * 修改用户头像
     *
     * @param adminName 用户ID
     * @param avatar 头像地址
     * @return 结果
     */
    public boolean updateAdminAvatar(String adminName, String avatar)
    {
        return adminMapper.updateAdminAvatar(adminName, avatar) > 0;
    }

    /**
     * 重置用户密码
     *
     * @param admin 用户信息
     * @return 结果
     */
    @Override
    public int resetPwd(FlyAdmin admin)
    {
        return adminMapper.updateAdmin(admin);
    }

    /**
     * 重置用户密码
     *
     * @param adminName 用户名
     * @param password 密码
     * @return 结果
     */
    @Override
    public int resetAdminPwd(String adminName, String password)
    {
        return adminMapper.resetAdminPwd(adminName, password);
    }

    /////////////////////////////////
    ///////        查詢       ////////
    /////////////////////////////////

    /**
     * 根据条件分页查询用户列表
     * 
     * @param admin 用户信息
     * @return 用户信息集合信息
     */
    @Override
    @DataScope(deptAlias = "d", adminAlias = "u")
    public Pager<FlyAdmin> selectAdminPager(FlyAdmin admin, Integer page, Integer pageSize, String sort, String order)
    {
        Pager<FlyAdmin> pager=new Pager(page,pageSize);
        pager.setEntity(admin);
        pager.setList(adminMapper.selectAdminPager(pager));
        pager.setTotal(adminMapper.queryAdminTotal(pager));
        return pager;
    }

    /**
     * 通过用户名查询用户
     * 
     * @param adminName 用户名
     * @return 用户对象信息
     */
    @Override
    public FlyAdmin selectAdminByName(String adminName)
    {

        FlyAdmin admin = adminMapper.selectAdminByName(adminName);

        FlyDept dept=deptMapper.selectDeptById(admin.getDeptId());
        if(dept!=null){
            admin.setDept(dept);
        }
        List<FlyRole> role=adminRoleMapper.selectAdminRoleByAdminId(admin.getAdminId());
        if(role != null){
            admin.setRoles(role);
        }
        return admin;
    }

    /**
     * 通过用户ID查询用户
     * 
     * @param adminId 用户ID
     * @return 用户对象信息
     */
    @Override
    public AdminQueryDTO findAdminById(Long adminId)
    {
        AdminQueryDTO dto = adminMapper.selectAdminById(adminId);
        return dto;
    }

    /**
     * 查询用户所属角色组
     * 
     * @param adminName 用户名
     * @return 结果
     */
    @Override
    public String selectAdminRoleGroup(String adminName)
    {
        List<FlyRole> list = roleMapper.selectRolesByAdminName(adminName);
        StringBuffer idsStr = new StringBuffer();
        for (FlyRole role : list)
        {
            idsStr.append(role.getRoleName()).append(",");
        }
        if (StrUtils.isNotEmpty(idsStr.toString()))
        {
            return idsStr.substring(0, idsStr.length() - 1);
        }
        return idsStr.toString();
    }

    /**
     * 查询用户所属岗位组
     * 
     * @param adminName 用户名
     * @return 结果
     */
    @Override
    public String selectAdminPostGroup(String adminName)
    {
        List<FlyPost> list = postMapper.selectPostsByAdminName(adminName);
        StringBuffer idsStr = new StringBuffer();
        for (FlyPost post : list)
        {
            idsStr.append(post.getPostName()).append(",");
        }
        if (StrUtils.isNotEmpty(idsStr.toString()))
        {
            return idsStr.substring(0, idsStr.length() - 1);
        }
        return idsStr.toString();
    }

    /**
     * 校验用户名称是否唯一
     * 
     * @param adminName 用户名称
     * @return 结果
     */
    @Override
    public String checkAdminNameUnique(String adminName)
    {
        int count = adminMapper.checkAdminNameUnique(adminName);
        if (count > 0)
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验用户名称是否唯一
     *
     * @param admin 用户信息
     * @return
     */
    @Override
    public String checkPhoneUnique(FlyAdmin admin)
    {
        Long userId = StrUtils.isNull(admin.getAdminId()) ? -1L : admin.getAdminId();
        FlyAdmin info = adminMapper.checkPhoneUnique(admin.getPhonenumber());
        if (StrUtils.isNotNull(info) && info.getAdminId().longValue() != userId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验email是否唯一
     *
     * @param admin 用户信息
     * @return
     */
    @Override
    public String checkEmailUnique(FlyAdmin admin)
    {
        Long userId = StrUtils.isNull(admin.getAdminId()) ? -1L : admin.getAdminId();
        FlyAdmin info = adminMapper.checkEmailUnique(admin.getEmail());
        if (StrUtils.isNotNull(info) && info.getAdminId().longValue() != userId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验用户是否允许操作
     * 
     * @param admin 用户信息
     */
    public void checkAdminAllowed(FlyAdmin admin)
    {
        if (StrUtils.isNotNull(admin.getAdminId()) && admin.isAdmin())
        {
            throw new CustomException("不允许操作超级管理员用户");
        }
    }

}
