package com.flycms.modules.notify.domain.dto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

/**
 * 系统邮件模板设置数据传输对象 fly_email_templet
 * 
 * @author admin
 * @date 2020-11-09
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class EmailTempletDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @Excel(name = "ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** 模板代码 */
    @Excel(name = "模板代码")
    private String tpCode;
    /** 邮件标题 */
    @Excel(name = "邮件标题")
    private String title;
    /** 邮件内容 */
    @Excel(name = "邮件内容")
    private String content;

}
