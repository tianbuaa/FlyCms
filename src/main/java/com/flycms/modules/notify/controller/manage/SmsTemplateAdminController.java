package com.flycms.modules.notify.controller.manage;

import com.flycms.modules.notify.service.ISmsTemplateService;
import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.notify.domain.SmsTemplate;
import com.flycms.modules.notify.domain.dto.SmsTemplateDto;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

/**
 * 短信模板Controller
 * 
 * @author admin
 * @date 2020-05-27
 */
@RestController
@RequestMapping("/system/notify/SmsTemplate")
public class SmsTemplateAdminController extends BaseController
{
    @Autowired
    private ISmsTemplateService smsTemplateService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增短信模板
     */
    @PreAuthorize("@ss.hasPermi('notify:SmsTemplate:add')")
    @Log(title = "短信模板", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SmsTemplate smsTemplate)
    {
        if (UserConstants.NOT_UNIQUE.equals(smsTemplateService.checkSmsTemplateSignIdUnique(smsTemplate.getSignId())))
        {
            return AjaxResult.error("新增短信模板'" + smsTemplate.getSignId() + "'失败，签名id已存在");
        }
        if (UserConstants.NOT_UNIQUE.equals(smsTemplateService.checkSmsTemplateTemplateNameUnique(smsTemplate.getTemplateName())))
        {
            return AjaxResult.error("新增短信模板'" + smsTemplate.getTemplateName() + "'失败，模板名称已存在");
        }
        return toAjax(smsTemplateService.insertSmsTemplate(smsTemplate));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除短信模板
     */
    @PreAuthorize("@ss.hasPermi('notify:SmsTemplate:remove')")
    @Log(title = "短信模板", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(smsTemplateService.deleteSmsTemplateByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改短信模板
     */
    @PreAuthorize("@ss.hasPermi('notify:SmsTemplate:edit')")
    @Log(title = "短信模板", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SmsTemplate smsTemplate)
    {
        return toAjax(smsTemplateService.updateSmsTemplate(smsTemplate));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询短信模板列表
     */
    @PreAuthorize("@ss.hasPermi('notify:SmsTemplate:list')")
    @GetMapping("/list")
    public TableDataInfo list(SmsTemplate smsTemplate,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "create_time") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<SmsTemplateDto> pager = smsTemplateService.selectSmsTemplatePager(smsTemplate, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出短信模板列表
     */
    @PreAuthorize("@ss.hasPermi('notify:SmsTemplate:export')")
    @Log(title = "短信模板", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SmsTemplate smsTemplate,
            @RequestParam(defaultValue = "1") Integer pageNum,
            @RequestParam(defaultValue = "10") Integer pageSize,
            @Sort @RequestParam(defaultValue = "id") String sort,
            @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<SmsTemplateDto> pager = smsTemplateService.selectSmsTemplatePager(smsTemplate, pageNum, pageSize, sort, order);
        ExcelUtil<SmsTemplateDto> util = new ExcelUtil<SmsTemplateDto>(SmsTemplateDto.class);
        return util.exportExcel(pager.getList(), "SmsTemplate");
    }

    /**
     * 获取短信模板详细信息
     */
    @PreAuthorize("@ss.hasPermi('notify:SmsTemplate:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(smsTemplateService.selectSmsTemplateById(id));
    }
}
