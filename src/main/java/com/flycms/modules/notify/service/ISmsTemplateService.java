package com.flycms.modules.notify.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.notify.domain.SmsTemplate;
import com.flycms.modules.notify.domain.dto.SmsTemplateDto;

/**
 * 短信模板Service接口
 * 
 * @author admin
 * @date 2020-05-27
 */
public interface ISmsTemplateService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增短信模板
     *
     * @param smsTemplate 短信模板
     * @return 结果
     */
    public int insertSmsTemplate(SmsTemplate smsTemplate);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除短信模板
     *
     * @param ids 需要删除的短信模板ID
     * @return 结果
     */
    public int deleteSmsTemplateByIds(Long[] ids);

    /**
     * 删除短信模板信息
     *
     * @param id 短信模板ID
     * @return 结果
     */
    public int deleteSmsTemplateById(Long id);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改短信模板
     *
     * @param smsTemplate 短信模板
     * @return 结果
     */
    public int updateSmsTemplate(SmsTemplate smsTemplate);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验签名id是否唯一
     *
     * @param sign_id 短信模板
     * @return 结果
     */
    public String checkSmsTemplateSignIdUnique(Long smsTemplate);

    /**
     * 校验模板名称是否唯一
     *
     * @param template_name 短信模板
     * @return 结果
     */
    public String checkSmsTemplateTemplateNameUnique(String smsTemplate);


    /**
     * 查询短信模板
     * 
     * @param id 短信模板ID
     * @return 短信模板
     */
    public SmsTemplate selectSmsTemplateById(Long id);

    /**
     * 查询短信模板列表
     * 
     * @param smsTemplate 短信模板
     * @return 短信模板集合
     */
    public Pager<SmsTemplateDto> selectSmsTemplatePager(SmsTemplate smsTemplate, Integer page, Integer limit, String sort, String order);
}
