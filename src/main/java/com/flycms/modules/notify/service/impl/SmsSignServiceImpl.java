package com.flycms.modules.notify.service.impl;

import com.flycms.common.utils.DateUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.notify.domain.SmsSign;
import com.flycms.modules.notify.mapper.SmsSignMapper;
import com.flycms.modules.notify.service.ISmsSignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.modules.notify.domain.dto.SmsSignDto;

import java.util.ArrayList;
import java.util.List;

/**
 * 短信签名Service业务层处理
 * 
 * @author admin
 * @date 2020-05-27
 */
@Service
public class SmsSignServiceImpl implements ISmsSignService
{
    @Autowired
    private SmsSignMapper smsSignMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增短信签名
     *
     * @param smsSign 短信签名
     * @return 结果
     */
    @Override
    public int insertSmsSign(SmsSign smsSign)
    {
        smsSign.setId(SnowFlakeUtils.nextId());
        smsSign.setCreateTime(DateUtils.getNowDate());
        return smsSignMapper.insertSmsSign(smsSign);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除短信签名
     *
     * @param ids 需要删除的短信签名ID
     * @return 结果
     */
    @Override
    public int deleteSmsSignByIds(Long[] ids)
    {
        return smsSignMapper.deleteSmsSignByIds(ids);
    }

    /**
     * 删除短信签名信息
     *
     * @param id 短信签名ID
     * @return 结果
     */
    @Override
    public int deleteSmsSignById(Long id)
    {
        return smsSignMapper.deleteSmsSignById(id);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改短信签名
     *
     * @param smsSign 短信签名
     * @return 结果
     */
    @Override
    public int updateSmsSign(SmsSign smsSign)
    {
        return smsSignMapper.updateSmsSign(smsSign);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询短信签名
     * 
     * @param id 短信签名ID
     * @return 短信签名
     */
    @Override
    public SmsSign selectSmsSignById(Long id)
    {
        return smsSignMapper.selectSmsSignById(id);
    }


    /**
     * 查询短信签名列表
     *
     * @param smsSign 短信签名
     * @return 短信签名
     */
    @Override
    public Pager<SmsSignDto> selectSmsSignPager(SmsSign smsSign, Integer page, Integer pageSize, String sort, String order)
    {
        Pager<SmsSignDto> pager=new Pager(page,pageSize);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(smsSign);

        List<SmsSign> smsSignList=smsSignMapper.selectSmsSignPager(pager);
        List<SmsSignDto> dtolsit = new ArrayList<SmsSignDto>();
        smsSignList.forEach(smsSigns -> {
            SmsSignDto smsSignDto = new SmsSignDto();
            smsSignDto.setId(smsSigns.getId());
            smsSignDto.setApiId(smsSigns.getApiId());
            smsSignDto.setSignName(smsSigns.getSignName());
            smsSignDto.setStatus(smsSigns.getStatus());
            dtolsit.add(smsSignDto);
        });
        pager.setList(dtolsit);
        pager.setTotal(smsSignMapper.querySmsSignTotal(pager));
        return pager;
    }

}
