package com.flycms.modules.monitor.service;

import java.util.List;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.monitor.domain.FlyJobLog;

/**
 * 定时任务调度日志信息信息 服务层
 * 
 * @author kaifei sun
 */
public interface IFlyJobLogService
{
    /////////////////////////////////
    ////////       增加       ////////
    /////////////////////////////////
    /**
     * 新增任务日志
     *
     * @param jobLog 调度日志信息
     */
    public void addJobLog(FlyJobLog jobLog);
    /////////////////////////////////
    ////////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除调度日志信息
     *
     * @param logIds 需要删除的日志ID
     * @return 结果
     */
    public int deleteJobLogByIds(Long[] logIds);

    /**
     * 删除任务日志
     *
     * @param jobId 调度日志ID
     * @return 结果
     */
    public int deleteJobLogById(Long jobId);

    /**
     * 清空任务日志
     */
    public void cleanJobLog();

    /////////////////////////////////
    ////////        修改      ////////
    /////////////////////////////////

    /////////////////////////////////
    ////////        查詢      ////////
    /////////////////////////////////
    /**
     * 通过调度任务日志ID查询调度信息
     *
     * @param jobLogId 调度任务日志ID
     * @return 调度任务日志对象信息
     */
    public FlyJobLog selectJobLogById(Long jobLogId);

    /**
     * 获取quartz调度器日志的计划任务翻页
     *
     * @param jobLog 调度日志信息
     * @param page
     * @param limit
     * @param sort
     * @param order
     * @return
     */
    public Pager<FlyJobLog> selectJobLogPager(FlyJobLog jobLog, Integer page, Integer limit, String sort, String order);

    /**
     * 查询所有调度任务日志
     *
     * @return 调度任务日志列表
     */
    public List<FlyJobLog> exportJobLogList(FlyJobLog jobLog);
}
