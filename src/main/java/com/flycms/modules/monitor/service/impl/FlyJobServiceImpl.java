package com.flycms.modules.monitor.service.impl;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;

import com.flycms.common.constant.ScheduleConstants;
import com.flycms.common.exception.job.TaskException;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.job.CronUtils;
import com.flycms.common.utils.job.ScheduleUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.site.domain.SiteLinks;
import com.flycms.modules.site.domain.dto.SiteLinksDTO;
import org.quartz.JobDataMap;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.flycms.modules.monitor.domain.FlyJob;
import com.flycms.modules.monitor.mapper.FlyJobMapper;
import com.flycms.modules.monitor.service.IFlyJobService;

/**
 * 定时任务调度信息 服务层
 * 
 * @author kaifei sun
 */
@Service
public class FlyJobServiceImpl implements IFlyJobService
{
    @Autowired
    private Scheduler scheduler;

    @Autowired
    private FlyJobMapper jobMapper;

    /////////////////////////////////
    ////////       增加       ////////
    /////////////////////////////////
    /**
     * 新增任务
     *
     * @param job 调度信息 调度信息
     */
    @Override
    @Transactional
    public int insertJob(FlyJob job) throws SchedulerException, TaskException
    {
        job.setStatus(ScheduleConstants.Status.PAUSE.getValue());
        int rows = jobMapper.insertJob(job);
        if (rows > 0)
        {
            ScheduleUtils.createScheduleJob(scheduler, job);
        }
        return rows;
    }
    /////////////////////////////////
    ////////        刪除      ////////
    /////////////////////////////////

    /**
     * 删除任务后，所对应的trigger也将被删除
     *
     * @param job 调度信息
     */
    @Override
    @Transactional
    public int deleteJob(FlyJob job) throws SchedulerException
    {
        Long jobId = job.getJobId();
        String jobGroup = job.getJobGroup();
        int rows = jobMapper.deleteJobById(jobId);
        if (rows > 0)
        {
            scheduler.deleteJob(ScheduleUtils.getJobKey(jobId, jobGroup));
        }
        return rows;
    }

    /**
     * 批量删除调度信息
     *
     * @param jobIds 需要删除的任务ID
     * @return 结果
     */
    @Override
    @Transactional
    public void deleteJobByIds(Long[] jobIds) throws SchedulerException
    {
        for (Long jobId : jobIds)
        {
            FlyJob job = jobMapper.selectJobById(jobId);
            deleteJob(job);
        }
    }
    /////////////////////////////////
    ////////        修改      ////////
    /////////////////////////////////
    /**
     * 更新任务的时间表达式
     *
     * @param job 调度信息
     */
    @Override
    @Transactional
    public int updateJob(FlyJob job) throws SchedulerException, TaskException
    {
        FlyJob properties = selectJobById(job.getJobId());
        int rows = jobMapper.updateJob(job);
        if (rows > 0)
        {
            updateSchedulerJob(job, properties.getJobGroup());
        }
        return rows;
    }

    /**
     * 更新任务
     *
     * @param job 任务对象
     * @param jobGroup 任务组名
     */
    public void updateSchedulerJob(FlyJob job, String jobGroup) throws SchedulerException, TaskException
    {
        Long jobId = job.getJobId();
        // 判断是否存在
        JobKey jobKey = ScheduleUtils.getJobKey(jobId, jobGroup);
        if (scheduler.checkExists(jobKey))
        {
            // 防止创建时存在数据问题 先移除，然后在执行创建操作
            scheduler.deleteJob(jobKey);
        }
        ScheduleUtils.createScheduleJob(scheduler, job);
    }

    /**
     * 任务调度状态修改
     *
     * @param job 调度信息
     */
    @Override
    @Transactional
    public int changeStatus(FlyJob job) throws SchedulerException
    {
        int rows = 0;
        String status = job.getStatus();
        if (ScheduleConstants.Status.NORMAL.getValue().equals(status))
        {
            rows = resumeJob(job);
        }
        else if (ScheduleConstants.Status.PAUSE.getValue().equals(status))
        {
            rows = pauseJob(job);
        }
        return rows;
    }
    /////////////////////////////////
    ////////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验cron表达式是否有效
     *
     * @param cronExpression 表达式
     * @return 结果
     */
    @Override
    public boolean checkCronExpressionIsValid(String cronExpression)
    {
        return CronUtils.isValid(cronExpression);
    }

    /**
     * 项目启动时，初始化定时器 主要是防止手动修改数据库导致未同步到定时任务处理（注：不能手动修改数据库ID和任务组名，否则会导致脏数据）
     */
    @PostConstruct
    public void init() throws SchedulerException, TaskException
    {
        scheduler.clear();
        List<FlyJob> jobList = jobMapper.selectJobAll();
        for (FlyJob job : jobList)
        {
            ScheduleUtils.createScheduleJob(scheduler, job);
        }
    }


    /**
     * 通过调度任务ID查询调度信息
     *
     * @param jobId 调度任务ID
     * @return 调度任务对象信息
     */
    @Override
    public FlyJob selectJobById(Long jobId)
    {
        return jobMapper.selectJobById(jobId);
    }

    /**
     * 获取quartz调度器的计划任务列表翻页
     *
     * @param job
     * @param page
     * @param pageSize
     * @param sort
     * @param order
     * @return
     */
    @Override
    public Pager<FlyJob> selectJobPager(FlyJob job, Integer page, Integer pageSize, String sort, String order)
    {
        Pager<FlyJob> pager=new Pager(page,pageSize);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(job);
        pager.setList(jobMapper.selectJobPager(pager));
        pager.setTotal(jobMapper.queryJobTotal(pager));
        return pager;
    }

    /**
     * 查询需要导出调度任务
     *
     * @param job 调度任务信息
     * @return 调度任务列表
     */
    public List<FlyJob> exportJobList(FlyJob job) {
        return jobMapper.exportJobList(job);
    }

    /**
     * 暂停任务
     * 
     * @param job 调度信息
     */
    @Override
    @Transactional
    public int pauseJob(FlyJob job) throws SchedulerException
    {
        Long jobId = job.getJobId();
        String jobGroup = job.getJobGroup();
        job.setStatus(ScheduleConstants.Status.PAUSE.getValue());
        int rows = jobMapper.updateJob(job);
        if (rows > 0)
        {
            scheduler.pauseJob(ScheduleUtils.getJobKey(jobId, jobGroup));
        }
        return rows;
    }

    /**
     * 恢复任务
     * 
     * @param job 调度信息
     */
    @Override
    @Transactional
    public int resumeJob(FlyJob job) throws SchedulerException
    {
        Long jobId = job.getJobId();
        String jobGroup = job.getJobGroup();
        job.setStatus(ScheduleConstants.Status.NORMAL.getValue());
        int rows = jobMapper.updateJob(job);
        if (rows > 0)
        {
            scheduler.resumeJob(ScheduleUtils.getJobKey(jobId, jobGroup));
        }
        return rows;
    }




    /**
     * 立即运行任务
     * 
     * @param job 调度信息
     */
    @Override
    @Transactional
    public void run(FlyJob job) throws SchedulerException
    {
        Long jobId = job.getJobId();
        String jobGroup = job.getJobGroup();
        FlyJob properties = selectJobById(job.getJobId());
        // 参数
        JobDataMap dataMap = new JobDataMap();
        dataMap.put(ScheduleConstants.TASK_PROPERTIES, properties);
        scheduler.triggerJob(ScheduleUtils.getJobKey(jobId, jobGroup), dataMap);
    }

}