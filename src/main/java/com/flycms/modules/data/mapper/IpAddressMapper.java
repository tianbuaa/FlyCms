package com.flycms.modules.data.mapper;

import java.util.LinkedList;
import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.data.domain.IpAddress;
import org.springframework.stereotype.Repository;

/**
 * IP地址库Mapper接口
 * 
 * @author admin
 * @date 2020-12-10
 */
@Repository
public interface IpAddressMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增IP地址库
     *
     * @param ipAddress IP地址库
     * @return 结果
     */
    public int insertIpAddress(IpAddress ipAddress);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除IP地址库
     *
     * @param id IP地址库ID
     * @return 结果
     */
    public int deleteIpAddressById(Long id);

    /**
     * 批量删除IP地址库
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteIpAddressByIds(Long[] ids);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改IP地址库
     *
     * @param ipAddress IP地址库
     * @return 结果
     */
    public int updateIpAddress(IpAddress ipAddress);


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询IP地址库
     * 
     * @param id IP地址库ID
     * @return IP地址库
     */
    public IpAddress findIpAddressById(Long id);

    /**
     * 查询当前大于等于起始段ip并且结束段ip小于当前最近一个ip起始段
     *
     * @param startIp  起始段ip
     * @param endIp   结束段ip
     *
     * @return
     */
    public LinkedList<IpAddress> findIpAddress(Long startIp, Long endIp);

    /**
     * 查询IP地址库
     *
     * @param ip IP地址
     * @return IP地址库
     */
    public IpAddress findSearchIpAddress(Long ip);

    /**
     * 查询IP地址库数量
     *
     * @param pager 分页处理类
     * @return IP地址库数量
     */
    public int queryIpAddressTotal(Pager pager);

    /**
     * 查询IP地址库列表
     * 
     * @param pager 分页处理类
     * @return IP地址库集合
     */
    public List<IpAddress> selectIpAddressPager(Pager pager);

    /**
     * 查询需要导出的IP地址库列表
     *
     * @param ipAddress IP地址库
     * @return IP地址库集合
     */
    public List<IpAddress> exportIpAddressList(IpAddress ipAddress);
}
