package com.flycms.modules.user.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.user.domain.UserAccount;
import com.flycms.modules.user.domain.dto.UserAccountDTO;

import java.util.List;

/**
 * 用户账户Service接口
 * 
 * @author admin
 * @date 2020-12-04
 */
public interface IUserAccountService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增用户账户
     *
     * @param userAccount 用户账户
     * @return 结果
     */
    public int insertUserAccount(UserAccount userAccount);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除用户账户
     *
     * @param userIds 需要删除的用户账户ID
     * @return 结果
     */
    public int deleteUserAccountByIds(Long[] userIds);

    /**
     * 删除用户账户信息
     *
     * @param userId 用户账户ID
     * @return 结果
     */
    public int deleteUserAccountById(Long userId);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改用户账户
     *
     * @param userAccount 用户账户
     * @return 结果
     */
    public int updateUserAccount(UserAccount userAccount);

    /**
     * 更新用户积分
     *
     * @param calculate
     *        运算：plus是加+,reduce是减-
     * @param score
     *        积分数量
     * @param userId
     *        用户id
     * @return
     */
    public int updateUserAccountScore(String calculate, Integer score, Long userId);

    /**
     * 更新用户发布话题数量
     *
     * @param userId 用户id
     * @return 结果
     */
    public int updateUserTopic(Long userId);

    /**
     * 更新用户加入的小组数量
     *
     * @param userId 用户id
     * @return 结果
     */
    public int updateUserGroup(Long userId);

    /**
     * 更新用户所有粉丝数量
     *
     * @param userId 用户id
     * @return 结果
     */
    public int updateUserFans(Long userId);

    /**
     * 更新用户被关注数量
     *
     * @param userId 用户id
     * @return 结果
     */
    public int updateUserFollow(Long userId);

    /**
     * 更新用户关注话题数量
     *
     * @param userId 用户id
     * @return 结果
     */
    public int updateUserFollowTopic(Long userId);

    /**
     * 更新关注标签数量
     *
     * @param userId 用户id
     * @return 结果
     */
    public int updateUserFollowLabel(Long userId);
    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验用户id是否唯一
     *
     * @param userId 用户id
     * @return 结果
     */
    public String checkUserAccountUserIdUnique(Long userId);


    /**
     * 查询用户账户
     * 
     * @param userId 用户账户ID
     * @return 用户账户
     */
    public UserAccountDTO findUserAccountById(Long userId);

    /**
     * 查询用户账户列表
     * 
     * @param userAccount 用户账户
     * @return 用户账户集合
     */
    public Pager<UserAccountDTO> selectUserAccountPager(UserAccount userAccount, Integer page, Integer limit, String sort, String order);

    /**
     * 查询需要导出的用户账户列表
     *
     * @param userAccount 用户账户
     * @return 用户账户集合
     */
    public List<UserAccountDTO> exportUserAccountList(UserAccount userAccount);
}
