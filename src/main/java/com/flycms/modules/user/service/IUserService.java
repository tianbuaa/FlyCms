package com.flycms.modules.user.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.user.domain.User;
import com.flycms.modules.user.domain.dto.UserDTO;
import com.flycms.modules.user.domain.dto.UserInfoDTO;

import java.awt.image.BufferedImage;
import java.util.List;

/**
 * 用户管理Service接口
 *
 * @author admin
 * @date 2020-05-24
 */
public interface IUserService
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增用户管理
     *
     * @param user 用户管理
     * @return 结果
     */
    public int insertUser(User user);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除用户管理
     *
     * @param ids 需要删除的用户管理ID
     * @return 结果
     */
    public int deleteUserByIds(Long[] ids);

    /**
     * 删除用户管理信息
     *
     * @param id 用户管理ID
     * @return 结果
     */
    public int deleteFlyUserById(Long id);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改用户管理
     *
     * @param user 用户管理
     * @return 结果
     */
    public int updateUser(User user);

    /**
     * 修改用户头像
     *
     * @param user 用户信息
     * @param image  头像地址
     * @return
     */
    public int updateUserAvatar(User user, BufferedImage image);

    /**
     * 修改用户密码
     *
     * @param id 用户id
     * @param password  用户密码
     * @return
     */
    public int updatePassword(Long id,String password);
    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验用户名称是否唯一
     *
     * @param user 用户管理
     * @return 结果
     */
    public String checkUsernameUnique(User user);

    /**
     * 校验用户手机号码是否唯一
     *
     * @param user 用户
     * @return 结果
     */
    public String checkUserMobileUnique(User user);

    /**
     * 校验邮箱是否唯一
     *
     * @param user 用户
     * @return 结果
     */
    public String checkUserEmailUnique(User user);

    /**
     * 校验短网址是否唯一
     *
     * @param shortUrl 用户
     * @return 结果
     */
    public String checkUserShorturlUnique(String shortUrl);

    /**
     * 查询用户短域名是否被占用
     *
     * @return
     */
    public String shortUrl();
    /**
     * 查询用户管理
     *
     * @param id 用户管理ID
     * @return 用户管理
     */
    public User findUserById(Long id);

    /**
     * 按shortUrl查询用户信息
     *
     * @param shortUrl
     * @return
     */
    public User findUserByShorturl(String shortUrl);

    /**
     * 查询用户管理
     *
     * @param id 用户管理ID
     * @return 用户管理
     */
    public UserInfoDTO findUserInfoById(Long id);

    /**
     * 按username查询用户管理详细信息
     *
     * @param username
     * @return
     */
    public User findUserByUsername(String username);

    /**
     * 按手机号码查询用户管理详细信息
     *
     * @param mobile
     * @return
     */
    public User findUserByMobile(String mobile);

    /**
     * 按邮箱查询用户管理详细信息
     *
     * @param email
     * @return
     */
    public User findUserByEmail(String email);

    /**
     * 查询用户管理列表
     *
     * @param user 用户管理
     * @return 用户管理集合
     */
    public Pager<UserDTO> selectUserPager(User user, Integer page, Integer limit, String sort, String order);

    /**
     * 查询需要导出的用户列表
     *
     * @param user 用户信息
     * @return 用户集合
     */
    public List<UserDTO> exportUserList(User user);
}
