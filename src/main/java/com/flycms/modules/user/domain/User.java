package com.flycms.modules.user.domain;

import java.util.Date;
import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 用户对象 fly_user
 * 
 * @author kaifei sun
 * @date 2020-10-30
 */
@Data
public class User extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;
    /**短域名 */
    private String shortUrl;
    /** 账号 */
    private String username;
    /** 密码 */
    private String password;
    /** 手机号码 */
    private String mobile;
    /** 邮箱 */
    private String email;
    /** 性别 */
    private Integer gender;
    /** 生日 */
    private Date birthday;
    /** 个性签名 */
    private String signature;
    /** 个人介绍 */
    private String about;
    /** 登录时间 */
    private Date lastLoginTime;
    /** 登录IP */
    private String lastLoginIp;
    /** 用户层级 0 普通用户，1 VIP用户，2 区域代理用户 */
    private Integer userLevel;
    /** 用户昵称或网络名称 */
    private String nickname;
    /** 用户头像图片 */
    private String avatar;
    /** 审核 0 可用, 1 禁用, 2 注销 */
    private Integer status;
    /** 用户余额 */
    private Double balance;
    /** 积分 */
    private Integer score;
    /** 经验值 */
    private Integer exp;
    /** 话题数量 */
    private Integer countTopic;
    /** 加入小组数 */
    private Integer countGroup;
    /** 粉丝数 */
    private Integer countFans;
    /** 被注数 */
    private Integer countFollow;
    /** 关注话题数量 */
    private Integer countFollowTopic;
    /** 关注标签数量 */
    private Integer countFollowLabel;
    /** 逻辑删除 */
    private Integer deleted;

}
