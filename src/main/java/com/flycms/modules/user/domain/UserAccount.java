package com.flycms.modules.user.domain;

import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 用户账户对象 fly_user_account
 * 
 * @author admin
 * @date 2020-12-04
 */
@Data
public class UserAccount extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 用户id */
    private Long userId;
    /** 用户余额 */
    private Double balance;
    /** 积分 */
    private Integer score;
    /** 经验值 */
    private Integer exp;
    /** 话题数量 */
    private Integer countTopic;
    /** 加入小组数 */
    private Integer countGroup;
    /** 粉丝数 */
    private Integer countFans;
    /** 被注数 */
    private Integer countFollow;
    /** 关注话题数量 */
    private Integer countFollowTopic;
    /** 关注标签数量 */
    private Integer countFollowLabel;
}
