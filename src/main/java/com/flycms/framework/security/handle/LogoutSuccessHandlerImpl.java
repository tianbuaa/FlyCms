package com.flycms.framework.security.handle;

import com.alibaba.fastjson.JSON;
import com.flycms.common.constant.Constants;
import com.flycms.common.constant.HttpStatus;
import com.flycms.common.utils.ServletUtils;
import com.flycms.common.utils.StrUtils;
import com.flycms.framework.manager.AsyncManager;
import com.flycms.framework.manager.factory.AsyncFactory;
import com.flycms.framework.security.LoginAdmin;
import com.flycms.framework.security.service.TokenService;
import com.flycms.framework.web.domain.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 自定义退出处理类 返回成功
 * 
 * @author kaifei sun
 */
@Configuration
public class LogoutSuccessHandlerImpl implements LogoutSuccessHandler
{
    @Autowired
    private TokenService tokenService;

    /**
     * 退出处理
     * 
     * @return
     */
    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws IOException, ServletException
    {
        LoginAdmin loginAdmin = tokenService.getLoginAdmin(request);
        if (StrUtils.isNotNull(loginAdmin))
        {
            String adminName = loginAdmin.getUsername();
            // 删除用户缓存记录
            tokenService.delLoginAdmin(loginAdmin.getToken());
            // 记录用户退出日志
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(adminName, Constants.LOGOUT, "退出成功"));
        }
        ServletUtils.renderString(response, JSON.toJSONString(AjaxResult.error(HttpStatus.SUCCESS, "退出成功")));
    }
}
