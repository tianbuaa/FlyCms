package com.flycms.framework.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 读取项目相关配置
 * 
 * @author kaifei sun
 */
@Component
@ConfigurationProperties(prefix = "flycms")
public class FlyMallProperties
{
    /** 项目名称 */
    private String name;

    /** 版本 */
    private String version;

    /** 版权年份 */
    private String copyrightYear;

    /** 实例演示开关 */
    private boolean demoEnabled;

    /** Window路径 */
    private static String filePathWindow;

    /** 上传路径 */
    private static String filePathLinux;

    /** 获取地址开关 */
    private static boolean addressEnabled;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    public String getCopyrightYear()
    {
        return copyrightYear;
    }

    public void setCopyrightYear(String copyrightYear)
    {
        this.copyrightYear = copyrightYear;
    }

    public boolean isDemoEnabled()
    {
        return demoEnabled;
    }

    public void setDemoEnabled(boolean demoEnabled)
    {
        this.demoEnabled = demoEnabled;
    }

    public static String getFilePathWindow()
    {
        return filePathWindow;
    }

    public void setFilePathWindow(String filePathWindow)
    {
        FlyMallProperties.filePathWindow = filePathWindow;
    }

    public static String getFilePathLinux()
    {
        return filePathLinux;
    }

    public void setFilePathLinux(String filePathLinux)
    {
        FlyMallProperties.filePathLinux = filePathLinux;
    }

    public static boolean isAddressEnabled()
    {
        return addressEnabled;
    }

    public void setAddressEnabled(boolean addressEnabled)
    {
        FlyMallProperties.addressEnabled = addressEnabled;
    }

    /**
     * 获取上传路径
     */
    public static String getUploadPath()
    {
        String profile = null;
        /** 本地文件上传路径 */
        String os = System.getProperty("os.name");
        //如果是Windows系统
        if (os.toLowerCase().startsWith("win")) {
            profile= getFilePathWindow();
        } else {  //linux 和mac
            profile= getFilePathLinux() ;
        }
        return profile;
    }

    /**
     * 获取头像上传路径
     */
    public static String getAvatarPath()
    {
        String profile = null;
        /** 本地文件上传路径 */
        String os = System.getProperty("os.name");
        //如果是Windows系统
        if (os.toLowerCase().startsWith("win")) {
            profile= getFilePathWindow();
        } else {  //linux 和mac
            profile= getFilePathLinux() ;
        }
        return profile + "/avatar";
    }

    /**
     * 获取下载路径
     */
    public static String getDownloadPath()
    {
        String profile = null;
        /** 本地文件上传路径 */
        String os = System.getProperty("os.name");
        //如果是Windows系统
        if (os.toLowerCase().startsWith("win")) {
            profile= getFilePathWindow();
        } else {  //linux 和mac
            profile= getFilePathLinux() ;
        }
        return profile + "/download/";
    }

}